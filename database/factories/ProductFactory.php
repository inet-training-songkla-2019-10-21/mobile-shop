<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $phones = $faker->randomElement(['iPhone 11', 'iPhone 8', 'iPhone 7', 'Sumsung S8', 'Sumsung S9', 'Sumsung S10',]);

    if (strpos($phones, 'iPhone') === 0) {
        $phone_type = 'iPhone';
        $img_phone = $faker->randomElement([
            'https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/iphone-11-pro-max-midnight-green-select-2019?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1566953859350',
            'https://drop.ndtv.com/TECH/product_database/images/913201720152AM_635_iphone_x.jpeg',
            'https://store.ais.co.th/media/catalog/product/i/p/iphone7plus-black-pureangles_2_3.jpg',
            'https://www.checkraka.com/uploaded/logo/a3/a339508567df2541235d959cd491a5b0.jpg',
            'https://images-na.ssl-images-amazon.com/images/I/61nlT53kRKL._SY445_.jpg',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYsPkteuVAB53sI0GvXGnpmfWFDRoqrpjbhKxnBEhfIi2mwfM39Q&s',
            'https://www.dailygizmo.tv/wp-content/uploads/2019/08/2019iphonesingle.jpg',
        ]);
    } else {
        $phone_type = 'Sumsung';
        $img_phone = $faker->randomElement([
            'https://www.checkraka.com/uploaded/logo/d8/d8cf96911871642a7de345e928002466.jpg',
            'https://media.4rgos.it/s/Argos/8877369_R_SET?$Main768$&w=620&h=620',
            'https://media-dtaconline.dtac.co.th/catalog/product/cache/e96373d1c57081d0b326a3dfa1f55e67/s/a/samsung_a70-black_3.png',
            'https://res.cloudinary.com/cenergy-innovation-limited-head-office/image/fetch/c_scale,q_70,f_auto,h_740/https://d1dtruvuor2iuy.cloudfront.net/media/catalog/product/p/w/pwb000243012-1.jpg',
            'https://www.thaimobilecenter.com/home/img_stock/201852_74291.jpg',
            'https://sv1.picz.in.th/images/2019/01/07/92qTw9.jpg',
        ]);
    }

    return [
        'name' => $phones,
        'type' => $phone_type,
        'picture' => $img_phone,
        'price' => $faker->numberBetween(5000, 10000),
        'amount' => $faker->numberBetween(1, 2),
        'description' => $faker->text,
    ];
});
