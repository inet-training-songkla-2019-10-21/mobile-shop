<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Book Store</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">
    <script src="{{asset('js/app.js')}}" defer></script>
    <style>
        body {
            background: #ffffff;
            color: black;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }
    </style>
</head>
<body>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{'Mobile Shop'}}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
    <div class="container">
            <main class="app-content">
                    <div class="app-title">
                      <div>
                        <h1><i class="fa fa-th-list"></i> <a href="{{route('admin.product')}}" class="main"><strong>Admin</strong></a></h1>
                        <p>Management Order</p>
                      </div>
                      <ul class="app-breadcrumb breadcrumb side">
                        <li class="breadcrumb-item active"><a href="{{route('admin.create.page')}}">Add Product</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.users')}}">All User</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.orders')}}">All Order</a></li>
                      </ul>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="tile">
                          <div class="tile-body">
                            <div class="table-responsive">
                              <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                        <tr>
                                                <th scope="col">User id</th>
                                                <th scope="col">Product name</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Amount</th>
                                                <th scope="col">Confirm</th>
                                                <th scope="col">Edit</th>
                                            </tr>
                                </thead>
                        <tbody>
                            @foreach($orders as $key => $order)
                            <tr>
                                <td>{{$order->user_id}}</td>
                                <td>{{$order->product_name}}</td>
                                <td>{{$order->price}}</td>
                                <td>{{$order->amount}}</td>
                                @if ($order->confirmed)
                                    <td>Comfirmed</td>
                                @else
                                    <td>Not Confirm</td>
                                @endif
                                <td><a href="{{route('admin.orders.getupdate', $order->id)}}" class="btn btn-outline-warning">แก้ไข</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </main>

</body>
</html>
