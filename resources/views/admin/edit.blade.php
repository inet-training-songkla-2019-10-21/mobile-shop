<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mobile Shop</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
​
    <style>
        body {
            background: #ffffff;
            color: black;
            text-decoration: none;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }
        .row{
            background: #F8F9F9;
        }
​
    </style>
</head>
<body>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{'Mobile Shop'}}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
<div class="container">
    <div class="row">

    </div>
    <br><br>
    <div class="row justify-content-center">
        <div class="col-6" align="left" style="margin: 30px">
            <h2 style="color: black;">แบบฟอร์มแก้ไขผลิตภัณฑ์</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="{{route("admin.edit")}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="productname">ชื่อ</label>
                    <input
                        type="text" class="form-control"
                        name="name" id="name"
                        aria-describedby="bookHelp"
                        placeholder="ชื่อสินค้า่"
                        value="{{$product->name}}"
                        required
                    >
                    <small id="bookHelp" class="form-text text-muted">ชื่อสินค้าที่คุณจะบันทึก</small>
                </div>
                <div class="form-group">
                    <label for="producttype">ประเภท</label>
                    <select multiple class="form-control" id="producttype" name="type">
                        <option>Iphone</option>
                        <option>Samsung</option>

                    </select>
                </div>
                <div class="form-group">
                    <label for="description">คำบรรยาย</label>
                    <textarea type="text" class="form-control" id="description" name="description">{{$product->description}}</textarea>
                </div>
​                <div class="form-group">
                    <label for="author">จำนวน</label>
                    <input type="number" class="form-control" id="amount" value="{{$product->amount}}"  name="amount" placeholder="ราคา" required>
                </div>
                <div class="form-group">
                    <label for="author">ราคา</label>
                    <input type="number" class="form-control" id="price" value="{{$product->price}}"  name="price" placeholder="ราคา" required>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{$product->id}}">
                </div>
                <br>
                <div align="left" >
                    <button type="submit" class="btn btn-success">บันทึกการเปลี่ยนแปลง</button>
                    <a class="btn btn-danger" href="{{route('admin.product')}}" role="button">ยกเลิก</a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
