@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header"><i class="fas fa-shopping-basket"></i>ORDER</div>

                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">ลำดับ</th>
                                        <th scope="col">ชื่อสินค้า</th>
                                        <th scope="col">จำนวน</th>
                                        <th scope="col">ราคา</th>
                                        <th scope="col">ยืนยัน</th>
                                    </tr>

                                    <tbody>
                                        
                                            @foreach($orders as $key => $order)
                                            
                                                <tr>
                                                    <th scope="row">{{$key + 1}}</th>
                                                    
                                                    <td>{{$order->product_name}}</td>
                                                    <td>{{$order->amount}}</td>
                                                    <td>{{$order->price}}</td>
                                                    @if ($order->confirmed)
                                                    <td>Confirmed</td>
                                                    @else
                                                    <td>Not Confirm</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                    </tbody>

                                </thead> 
                            </table>
                            <div class="modal-footer">
                                    <a type="cancel" class="btn btn-danger" href="{{route('user.products')}}" class="card-link" style="margin: 5px">Close</a>         
                                </div>
                        </div>        
                    </div> 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
