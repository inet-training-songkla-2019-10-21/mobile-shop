@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1 class="h3 mb-4 text-gray-800">ORDER</h1>
                    <div class="card-body">
                      <h5 class="card-title">Details</h5>
                      
                      <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                    </div>
                    
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><div class="row">Product name<div class="col-md-11">{{$order->product_name}}</div><div class="col-md-1"><i class="fas fa fa-trash"></i></div></div></i></li>
                      <li class="list-group-item"><div class="row">Product price<div class="col-md-11">{{$order->price}}</div><div class="col-md-1"><i class="fas fa fa-trash"></i></div></div></i></li>
                      <!-- <li class="list-group-item">__________-<i class="fas fa fa-trash"></i></li>
                      <li class="list-group-item">______________-<i class="fas fa fa-trash"></i></li> -->
                    </ul>
                     <div class="modal-footer">
                        <a type="cancel" class="btn btn-danger" href="{{route('user.products')}}" class="card-link" style="margin: 5px">Close</a>         
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
