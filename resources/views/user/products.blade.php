@extends('layouts.app')

<style>
        .button {
          background-color: #375d8b; /* Green */
          border: none;
          color: white;
          padding: 5px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          cursor: pointer;
        }
        
        .button1 {border-radius: 8px;}
        
        .button2 {
          background-color: #547cad;
          color: white;
        }
        
        .button2:hover {
          background-color: #aec7e0 ; 
          color: black; 
        }
        </style>

@section('content')
<div class="container">
    <style>
                .md-pills .nav-link.active {
                  background-color:rgba(52, 164, 221);
                    color: aliceblue;

                }
    </style>

    <div class="row justify-content-center">
        <div class="col-12" style="background-color:rgb(255,255,255);">       
                <div class="row">
                        <div class="col-2" style="margin: 10px">
                            <a href="{{route('user.products')}}"><h2><b> PRODUCTS</b></h2></a>
                        </div>
              <ul class="nav md-pills flex-center flex-wrap mx-0" role="tablist">
                 <li class="nav-item" style="margin: 10px">
                  <a class="nav-link active font-weight-bold"  href="{{route('user.iphone')}}">iPhone</a>
                 </li>
                 <li class="nav-item" style="margin: 10px">
                  <a class="nav-link active font-weight-bold"  href="{{route('user.samsung')}}" >Sumsung</a>
                </li>
              </ul>
            </div>
            <div class="row">
                @foreach($products as $key => $product)
                                    
                    <div class="col-5" style="margin: 25px" >
                            <div class="card">                                
                                <div class="card-header" style="background-color:rgba(0,132,255,0.6);" ><b>{{$product->name}}</b></div>
                                <div class="row" style="margin: 10px">
                                    <div class="col-5">
                                        <img style="height:8rem;" class="rounded" 
                                            src="{{$product->picture}}"
                                            class="d-block w-1">
                                    </div>
                                    <div class="col-7">
                                        <div class="row">
                                            <div class="col-12" align="left"><b>BRAND : </b>{{$product->type}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12" align="left"><b>DESCRIPTION :</b> {{ Str::limit($product->description, $limit = 50, $end = '...') }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12" align="left"><b>PRICE :</b> {{$product->price}}฿</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-10" align="right"><a href="{{route('user.productsbuy', $product->id)}}" class="btn btn-outline-primary">Buy</a></div>
                                            
                                        </div>                     
                                    </div>
                                </div>
                            </div>  
                        </div>
                    
                @endforeach
            </div>             
        </div>
    </div>
</div>
@endsection