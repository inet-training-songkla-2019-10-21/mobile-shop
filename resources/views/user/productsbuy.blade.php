@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{route("user.submitOrder", $product)}}" method="post">
    {{ csrf_field() }}
    <div class="row justify-content-center">
        <div class="card">
      <div class="col-md-12">
          <div class="row">
                <div class="col-12" style="margin: 30px"><h2><b>PRODUCTS BUY</b></h2></div>
          </div>
          <div class="row" >
              <div class="co12" align="center" style="margin:20px">
                  <h4>{{$product->name}}</h4>
        <div class="row">
            <div class="col-12" align="center" style="margin: 30px">
                <img style="height:15rem;" class="rounded"
                src="{{$product->picture}}"                   
                alt="Card image cap">
            </div>
        </div>
        <div class="row">
            <div class="col-8 offset-2" align="left">
                <h3>Products Detail</h3>
                <p>{{$product->description}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-8 offset-2" align="left">
                <b><h5>BRAND :  </b>  {{$product->type}} </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-8 offset-2" align="left">
                <b><h5>PRICE :</b> {{$product->price}}฿</h5>
            </div>
        </div>

        <div class="row">
                <div class="col-4 offset-8" >
                    <a class="btn btn-danger" href="{{route('user.products')}}">Cancle</a>
                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Order</button>
                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Order</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                        You Order is Successfull..
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</form>
</div> 
@endsection