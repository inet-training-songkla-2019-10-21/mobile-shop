<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'User\ProductController@showallproduct')->name('home');

Route::get('/onechat', 'OneChatController@getInfo');
Route::get('/onechat/message/{msg}', 'OneChatController@sendMsg');

Route::get('/login/facebook', 'LoginController@redirectToProvider')->name('login/facebook');
Route::get('/login/facebook/callback', 'LoginController@handleProviderCallBack')->name('login/facebook');


Broadcast:: Routes();
require 'web/route_product.php';

