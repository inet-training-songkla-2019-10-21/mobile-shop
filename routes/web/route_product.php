<?php

//Admin
Route::group(['middleware' => ['admin']], function () {
    Route::get('/product', 'Admin\ProductController@adminProduct')->name('admin.product');
    Route::get('/usersadmin', 'Admin\UserController@alluser')->name('admin.users');
    Route::post('/product/create', 'Admin\ProductController@create')->name('admin.create');
    Route::get('/product/create', 'Admin\ProductController@createPage')->name('admin.create.page');
    Route::get('/product/delete/{id}', 'Admin\ProductController@delete')->name('admin.delete');
    Route::get('/product/edit/{id}', 'Admin\ProductController@editPage')->name('admin.edit.page');
    Route::post('/product/edit', 'Admin\ProductController@edit')->name('admin.edit');

    Route::get('/orders', 'Admin\OrderController@showOrders')->name('admin.orders');
    Route::get('/orders/update/{id}', 'Admin\OrderController@getupdateOrder')->name('admin.orders.getupdate');
    Route::post('/orders/update', 'Admin\OrderController@updateOrder')->name('admin.orders.update');


});

//User
Route::get('/user/products', 'User\ProductController@showallproduct')->name('user.products');
Route::get('/user/productsbuy/{id}', 'User\ProductController@productsbuy')->name('user.productsbuy');
Route::get('/user/orders', 'User\OrderController@showallorder')->name('user.showallorder');
Route::get('/user/iphone','User\ProductController@showiphone')->name('user.iphone');
Route::get('/user/samsung','User\ProductController@showsamsung')->name('user.samsung');

Route::get('/user/submitOrder/{product}', 'User\OrderController@submitOrder')->name('user.submitOrder');
Route::post('/user/submitOrder/{product}', 'User\OrderController@submitOrder')->name('user.submitOrder');
