<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Product;

class ProductTest extends TestCase
{
    public function testAdminCanSeeAllProduct(){
        $product = factory(Product::class,5)->create();
        $response = $this->get(route('admin.product'))->assertSuccessful();
        $response->assertSee($product[0]->name);
        $response->assertSee($product[1]->name);
        $response->assertSee($product[2]->name);
        $response->assertSee($product[3]->name);
        $response->assertSee($product[4]->name);
    }
    // public function testAdminCanAddNewProduct(){
        // $product = factory(Product::class)->make();
        // $this->get(route('admin.create.page'))->assertViewIs('admin.create')->assertStatus(200);
        // $this->post(route('admin.create'),[
        //     'name' => $product->name,
        //     'type' => $product->author,
        //     'price' => $product->price,
        //     'describe'=> $product->describe,
        //     'amount' => $product->amount,
        // ])->assertRedirect(route('admin.product'));
        // $this->assertDatabaseHas('products', [
        //     'name' => $product->name,
        //     'type' => $product->type,
        //     'price' => $product->price,
        //     'describe' => $product->describe,
        //     'amount' => $product->amount,
        // ]);
    // }
}
