<?php
 return [
    'client' => [
        'id' => env('FACEBOOK_CLIENT_ID'),
        'secret' => env('FACEBOOK_CLIENT_SECRET'),
        'version' => env('FACEBOOK_VERSION'),
    ],
 ];