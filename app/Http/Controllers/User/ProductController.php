<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function showallproduct()
    {
        $products = Product::all();

        return view('user.products', compact('products'));
    }

    public function productsbuy(Product $id)
    {
        return view('user.productsbuy', ['product' => $id]);
    }

    public function showiphone()
    {
        $products = Product::all();
        return view('user.iphone', compact('products'));
    }

    public function showsamsung()
    {
        $products = Product::all();

        return view('user.samsung', compact('products'));
    }
}
