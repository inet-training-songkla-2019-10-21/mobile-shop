<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function showallorder()
    {
        $orders = Order::where('user_id', Auth::user()->id)->get();
        return view('user.showallorder', compact('orders', $orders));
    }
    public function submitOrder(Product $product)
    {
        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->product_id = $product->id;
        $order->product_name = $product->name;
        $order->amount = $product->amount;
        $order->price = $product->price;
        $order->save();
        return view('user.submitOrder', compact('order'));
    }
}
