<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OneChatController extends Controller
{
    public function getInfo()
    {
        $data =[
            'headers' =>[
                'Authorization' => 'Bearer Acbe08b9cb05456ad94afdbe0b58fb74532d45ea399db47b8b214aa1531757745a5bb1c022831451683f153bb82c6988b',
                'Content-Type' => 'application/json',
            ],
            'json' =>[
                'bot_id' => 'Bf7b0e957841d5ee4ba52f3136eef9186'
            ]
            ];
            try{
                $client = new Client([
                    'base_uri' => 'https://chat-manage.one.th:8997/api/v1/',
                    'verify' => false,
                ]);

                $res = $client->request('POST','getlistroom', $data);
            }catch (ConnectException $e){

                return $e->getMessage();
            }
            return json_encode(json_decode($res->getBody()->getContents()));
    }

    public function sendMsg($msg)
    {
        $data = [
            'headers' =>[
                'Authorization' => 'Bearer Acbe08b9cb05456ad94afdbe0b58fb74532d45ea399db47b8b214aa1531757745a5bb1c022831451683f153bb82c6988b',
                'Content-Type' => 'application/json',
            ],
            'json' =>[
                "to"=> "U8a08f7004b7c5625a16e4cadd3a753cf",
                "bot_id"=> "Bf7b0e957841d5ee4ba52f3136eef9186",
                "type"=> "text",
                "message"=> $msg,
            ]
        ];

        try{
            $client = new Client([
                'base_uri' => 'https://chat-public.one.th:8034/api/v1/',
                'verify' => false,
            ]);

            $res = $client->request('POST','push_message', $data);
        }catch (ConnectException $e){

            return $e->getMessage();
        }
        return json_encode(json_decode($res->getBody()->getContents()));

    }
}
