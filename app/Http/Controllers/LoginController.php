<?php

namespace App\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()

    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
         $user = \App\User::where('email', $request->email)->get()->first();
         $user = \App\User::where([
         'email' => $request->email,
          ])->get(['password','name']);

         $isAuth = Auth::attempt(['email' => $request->email, 'password' => $request->password]);

         if(!$isAuth){
             return redirect()->back();
         }

         return redirect()->route('home');
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('book.index');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function handleProviderCallBack()
    {
        $facebookUser = Socialite::driver('facebook')->stateless()->user();

            $userExist = User::where('email',$facebookUser->email)->get()->first();

            if(empty($userExist)){

                $user = new User();
                $user->name = $facebookUser->name;
                $user->email = $facebookUser->email;
                $user->org_auth = 'facebook';
                $user->org_id = $facebookUser->id;
                $user->save();

                $facebook = new Facebook();
                $facebook->id = $facebookUser->id;
                $facebook->token = $facebookUser->token;
                $facebook->refresh_token = $facebookUser->refreshToken;
                $facebook->expires_in = $facebookUser->expiresIn;
                $facebook->avatar_original = $facebookUser->avatar_original;
                $facebook->created_by = $user->id ;
                $facebook->save();

                Auth::login($user);


            }else{
                Auth::login($userExist);
            }
            return redirect()->route('home');

    }

}
