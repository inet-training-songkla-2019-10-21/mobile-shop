<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
class ProductController extends Controller
{
    public function adminProduct()
    {
        $products = Product::all();
        return view('admin.product', compact('products'));
    }
    public function createPage()
    {
        return view('admin.create');
    }

    public function create(Request $request)
    {
        // dd($request->all());
        // dd($request->name);
        $product = new Product();
        $product->name = $request->name;
        $product->type = $request->type;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->amount = $request->amount;

        if (!$product->save()) {
            return redirect()->route('admin.create.page');
        }
        return redirect()->route('admin.product');
    }
    public function delete(Product $id){
        $id->delete();
        return redirect()->route('admin.product');
    }

    public function editPage($id)
    {
        $product = Product::find($id);
        // dd($book);
        return view('admin.edit', ['product' => $product]);
    }
    public function edit(Request $request)
    {
        //   dd($request->all());
        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->type = $request->type;
        $product->price =  $request->price;
        $product->description =  $request->description;
        $product->amount =  $request->amount;
        if (!$product->save()) {
            return redirect()->route('admin.edit.page');
        }
        return redirect()->route('admin.product');
    }
}
