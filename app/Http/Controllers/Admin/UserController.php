<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    public function alluser()
    {
        $users = User::all();
        return view('admin.users', compact('users'));
    }
}
