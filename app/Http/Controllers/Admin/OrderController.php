<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function showOrders()
    {
        $orders = Order::all();
        return view('admin.order.orders', compact('orders', $orders));
    }

    public function getupdateOrder($id)
    {
        $order = Order::find($id);
        return view('admin.order.update', ['order' => $order]);
    }

    public function updateOrder(Request $request)
    {

        $order = Order::find($request->id);
        $order->price = $request->price;
        $order->amount= $request->amount;
        $order->confirmed =  $request->confirmed;

        if (!$order->save()) {
            return redirect()->route('admin.order.getupdate');
        }
        return redirect()->route('admin.orders');
    }
}
